---
title: "cook | 早餐吃肉系列"
date: 2022-03-08T13:49:46+08:00
tags: ["cook"]
keywords: []
description: "土豆丝肉馅饼/鸡胸肉饼/卷"
slug: ""
---

>早餐吃肉系列
- 馄饨/紫菜馄饨/夏天花生酱冷馄饨
- 土豆丝/胡萝卜/葫芦丝/鸡蛋肉馅饼
- 鸡胸肉末/西兰花/胡萝卜饼
- 鸡胸肉卷+生菜


## 0. 馄饨
>家属爱吃；
>前天调好馅儿，第二天早上包四十个就够；或者包 100 个能吃三顿，再配点手抓饼很方便

- 手抓饼：三分钟即可；小火，不要烤焦；
- 热馄饨
    - 在碗里加汤底: 紫菜/香菜/香油/虾米；然后舀汤到碗里
    - 调肉馅
        - 多加一点白胡椒粉和盐
        - 葱水分两次加；
        - 加完后，再加五香粉、生抽、酱油、白胡椒粉、姜粉
        - 最后加盐、香油

        
- 冷馄饨
> 本文由 [简悦 SimpRead](http://ksria.com/simpread/) 转码， 原文地址 [mp.weixin.qq.com](https://mp.weixin.qq.com/s?__biz=MzA3ODc4NTMyNw==&mid=208413741&idx=1&sn=79c8414d896329c934b65400b7998ca4&scene=21#wechat_redirect)

```

*   但是不要用那种肉特别少皮特别薄的小馄饨，在过凉水的步骤中太容易破了；
    

*   花生酱一汤匙，我用的品牌就是超市常见的四季宝幼滑型
    
*   老抽半汤匙，生抽一汤匙，香醋一汤匙；
    
*   凉白开大概 50~80ml，看你对汤汁浓郁程度的喜好；  
    
*   辣椒油一汤匙，熟的白芝麻一汤匙；  
    
*   小葱一小把切葱花；  

    

**步骤：**

1. 调汤底：

先把花生酱、老抽、生抽放在一个碗里，用铁勺搅拌均匀。然后倒入凉白开和香醋，再一点点打匀拌开到没有结块，**放到冰箱冷藏过夜**。像调麻酱




先把花生酱拌开一点再加凉白开。如果一开始就把凉白开加进去，花生酱反而容易因为搅拌不到而打不匀。

提前一晚处理汤底再冷藏过夜，早上配合温热的馄饨，温度刚好。如果想早上来处理所有汤底内容的话，把凉白开提前一晚放冷藏就行，效果差不多的。

2. 煮馄饨：

因为用的是大馄饨，所以**煮的时候需要点三次水**，好像煮水饺一样：

一大锅水沸腾后放入冷冻的馄饨生坯，煮沸后加一小碗凉水，反复三次，**最后一次沸腾之后捞出馄饨，过凉水并沥干**。

*   点三次凉水是为了调整锅里的温度，让馅料充分煮熟，而馄饨皮又不至于太容易煮破；
    
*   煮熟之后过凉水是为了降温，并且让馄饨皮不容易粘连，和凉面的处理方法类似；
    
*   过凉水之后要充分沥干，这样汤底的味道不容易被影响。
    

3. 把步骤 1 做好的汤底淋到煮好的馄饨上，再淋辣椒油、撒葱花和白芝麻就可以咯！
```
## 1. 土豆丝/胡萝卜/葫芦丝/鸡蛋肉馅饼
做了两次, 总结问题经验

~ 第一次: 油.
~ 第二次: 不焦脆, 没味道

- 经验:
    - 不要加油
    - 电饼铛速脆档, 不要打小火; 两分钟后再揭锅翻面, 揭得早了就不焦
    - 没加孜然和盐芝麻
    - 没加鸡蛋液和面粉



好吃的正是土豆丝焦焦的感觉. 而且方便, 前天冷藏化肉, 早晨调料/擦土豆丝丝, 加调料淀粉即可



> 本文由 [简悦 SimpRead](http://ksria.com/simpread/) 转码， 原文地址 [www.xiachufang.com](https://www.xiachufang.com/recipe/103038570/)

> 【土豆丝红萝卜肉沫煎饼】1. 土豆，红萝卜切丝，打入鸡蛋，放适量盐，椒盐粉，孜然粉腌制五分钟；2. 个人比较爱吃肉肉，猪肉剁成肉沫，加入姜丝盐腌制 5 分钟；3. 把所有食材放在一个碗里，加入面粉，黑芝麻，搅拌均......



![](https://i2.chuimg.com/5ca83465856047f5afa99bbb731103fb_864w_1152h.jpg?imageView2/2/w/660/interlace/1/q/90) 
用料  
----

<table><tbody><tr><td><a href="/category/206/">土豆</a></td><td>1 个</td></tr><tr><td><a href="/category/394/">鸡蛋</a></td><td>1 只</td></tr><tr><td><a href="/category/1210/">红萝卜</a></td><td>半根</td></tr><tr><td><a href="/category/1805/">盐</a></td><td>适量</td></tr><tr><td><a href="/category/727/">面粉</a></td><td>适量</td></tr><tr><td><a href="/category/1003189/">肉沫</a></td><td>适量</td></tr></tbody></table>

土豆丝红萝卜肉沫煎饼的做法  
---------------

1.  土豆，红萝卜切丝，打入鸡蛋，放适量盐，椒盐粉，孜然粉腌制五分钟
    
    ![](https://i2.chuimg.com/c8fbe30e861c4f2793bee48c8dffd66a_864w_1152h.jpg?imageView2/2/w/300/interlace/1/q/90)
2.  个人比较爱吃肉肉，猪肉剁成肉沫，加入姜丝盐腌制 5 分钟

3.  把所有食材放在一个碗里，加入面粉，黑芝麻，搅拌均匀，不要太稀
    
    
4.  热锅，倒油，等油热后小火煎
    

5.  两面煎至金黄

    
## 2. 煎鸡胸肉
>简单而且嫩嫩嫩!
这个方法最简单: 
- 前一天解冻, 用料腌一晚上; 
- 第二天早晨拿出来用电饼称小火上下煎 4 分钟
        - 热火热油, 油一丢丢
        - 前两分钟加上盖, 后两分钟揭开盖关掉上盘

~ 经验
- 最好切小块, 比一整片口感好
- 面粉一定要能裹住鸡胸肉块儿, 宁多不少
- 小苏打让肉嫩起来, 不能不加
- 蛋清是以前不曾加过的
- 黑胡椒碎是灵魂, 盐可以不加
- 加调料的时候切记最后再放淀粉, 否则会结块儿, 得重新调制
- 以下用料是一斤鸡胸肉的调料量, 根据鸡胸肉具体用料减量
- 不要忘记马杀鸡, 也很关键, 按摩鸡胸肉, 有这一步和没有这一步, 还是有差别的
-  一次性多腌一点，然后分成小袋丢进冷冻盒

~ 用料  
* 鸡胸肉	1斤
* 玉米淀粉	 20克 三瓷勺
* 生抽	3勺
* 黑胡椒	2克
* 盐	1-2克（可不放）
* 橄榄油	5-10克
* 大蒜子	4瓣捣碎泥
* 可食用小苏打	2克
* 蛋清	1个


> 本文由 [简悦 SimpRead](http://ksria.com/simpread/) 转码， 原文地址 [www.xiachufang.com](https://www.xiachufang.com/recipe/104123173/)

> 1. 我买的是这种新鲜的鸡胸肉 将鸡胸肉洗干净以后用厨房纸巾吸干表面水份，不用太干哟！ 然后对半平着切，喜欢小一点的话在竖着切一刀；
> 2. 把切好的鸡胸肉放在无水无油的容器里，放入以上调料，然后给鸡胸肉做马杀, 大概 5 分钟左右，保证每一面都裹上调味料！ 
>  裹上保鲜膜放进冰箱冷藏至少 5 个小时

![](https://i2.chuimg.com/bd1c47e18ce24d25a89baba99933f999_1080w_864h.jpg?imageView2/2/w/660/interlace/1/q/90)


    



## 3. 鸡胸肉/西兰花/胡萝卜洋葱饼
~ 坑指南:

 - 做了一次, 竟然没有加西蓝花和胡萝卜, 真是太干太难吃了. 

- 没加黑胡椒粉和淀粉/洋葱丁, 这几样一个是嫩, 一个是提味, 竟然都没加, 好吃才怪

tips 
> 煎任任何肉类都不要加油 不然会很腻肉本身就会出油
> 鸡胸肉或者鸡肉一定要用清水泡半天去腥味

* 鸡胸肉打碎 西兰花胡萝卜用水煮两分钟混合打成泥
* 不要加蛋液 虽然加蛋液有助于粘合 但是不利于保存
* 煎好后冷却 冷冻保存 一周内吃完当早餐


> 本文由 [简悦 SimpRead](http://ksria.com/simpread/) 转码， 原文地址 [www.xiachufang.com](https://www.xiachufang.com/recipe/106568319/)

> 1. 鸡胸肉剁成肉沫➕洋葱丁➕玉米粒，倒入所有腌制料搅拌均匀，盖上保鲜膜腌制半小时，取适量肉泥整成小圆饼，少油小火慢煎至两面金黄即可出锅；

用料  
----

<table><tbody><tr><td>材料：</td><td></td></tr><tr><td><a href="https://www.xiachufang.com/category/1003687/">鸡胸肉</a></td><td>1 块</td></tr><tr><td><a href="https://www.xiachufang.com/category/1001/">洋葱</a></td><td>四分之一</td></tr><tr><td><a href="https://www.xiachufang.com/category/5318/">玉米粒</a></td><td>少许</td></tr><tr><td>腌制料：</td><td></td></tr><tr><td><a href="https://www.xiachufang.com/category/2399/">料酒</a></td><td>1 勺</td></tr><tr><td><a href="https://www.xiachufang.com/category/2362/">生抽</a></td><td>2 勺</td></tr><tr><td><a href="https://www.xiachufang.com/category/1012664/">耗油</a></td><td>1 勺</td></tr><tr><td><a href="https://www.xiachufang.com/category/2406/">老抽</a></td><td>几滴</td></tr><tr><td><a href="https://www.xiachufang.com/category/1526/">黑胡椒粉</a></td><td>半勺</td></tr><tr><td><a href="https://www.xiachufang.com/category/1925/">淀粉</a></td><td>少许</td></tr></tbody></table>



1.  鸡胸肉剁成肉沫➕洋葱丁➕玉米粒，倒入所有腌制料搅拌均匀，盖上保鲜膜腌制半小时，取适量肉泥整成小圆饼，少油小火慢煎至两面金黄即可出锅！  
    
    ![](https://i2.chuimg.com/b73fed1c98be4d0d9ae45367ed92c0f2_960w_1280h.jpg?imageView2/2/w/300/interlace/1/q/90)
2.  我一般一餐吃三个就够了！
    
    ![](https://i2.chuimg.com/afc54d4136b84de9865b2d5c3d3fabbf_960w_1280h.jpg?imageView2/2/w/300/interlace/1/q/90)
3.  真的好好吃！空气炸锅[烤箱](https://www.xiachufang.com/category/40057/)都可以，还可以不用放油！
    
4.  可以一次做一些整理成型，冷冻保存哦
    



## 鸡胸肉卷+生菜
唯一麻烦的是需要购买菠菜饼皮.也可以省略, 直接用生菜卷.加点番茄酱更好.

> 本文由 [简悦 SimpRead](http://ksria.com/simpread/) 转码， 原文地址 [www.xiachufang.com](https://www.xiachufang.com/recipe/104473313/)





用料  
----

<table><tbody><tr><td>菠菜卷饼（麦西恩）</td><td>两张</td></tr><tr><td><a href="https://www.xiachufang.com/category/1136/">鸡肉</a></td><td>一小块</td></tr><tr><td><a href="https://www.xiachufang.com/category/394/">鸡蛋</a></td><td>2 个</td></tr><tr><td>黄瓜 胡萝卜 生菜</td><td>适量</td></tr><tr><td><a href="https://www.xiachufang.com/category/2316/">牛奶</a></td><td>30 克</td></tr></tbody></table>

低脂食谱 12～鸡肉卷｜快手低卡吃不胖美食的做法  
--------------------------

1.  准备食材
    
    ![](https://i2.chuimg.com/2dd1688073cb40e18acbbb6d3846a152_2847w_2847h.jpg?imageView2/2/w/300/interlace/1/q/90)
2.  鸡肉切片
    
    ![](https://i2.chuimg.com/3f4f86da2e2a426cbd1f7717ce46711b_1177w_941h.jpg?imageView2/2/w/300/interlace/1/q/90)


6.  锅中刷油小火🔥煎至两面金黄
    
    ![](https://i2.chuimg.com/a684f15233484ad2b8872936137b719b_2454w_1840h.jpg?imageView2/2/w/300/interlace/1/q/90)

10.  一定要卷紧
    

11.  对半切开即可
    
    ![](https://i2.chuimg.com/205b117a0f104d5fa6c8e4a1a99a6a5a_2880w_2160h.jpg?imageView2/2/w/300/interlace/1/q/90)



