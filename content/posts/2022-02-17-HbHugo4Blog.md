
---
title: "技术 | hugo & gitlab 搭建博客"
date: 2022-02-15T23:39:50+08:00
draft: false
tags: ["技术"]
keywords: []
description: " hugo & gitlab 搭建博客"
slug: "hugo & gitlab 搭建博客"
---

## 0 Abastract:
通过 `hugo`& `Gitlab` 搭建博客 `Blog`


>博客搭建的主要步骤:

- 搭建: 用什么搭建毛坯房
- 存储: 存储在云端的什么位置
- 部署: 用什么来部署能让别人访问到
- 域名: 取一个容易访问到的名字

即
1.使用 Hugo/Hexo 等静态网站生成工具生成一个网站。
2.在 GitHub/ Gitlab 开一个仓库，将网站使用Git上传至仓库。
3.用Markdown写第一篇文章。通过Git推送到仓库后，网站自动更新发布上线！
4.配置域名解析(未做可省略)

具体到本博客

1. 使用 Hugo 静态网站生成工具, 生成网站, 通过 `hugo server` 命令可查询部署的网页
2. 新建 Gitlab 项目 project :qilin_craft/qilin_craft.gitlab.io, 并 clone 至本地；
3.  将 已建好的 Hugo blog 复制到`io` 中，git 推送至 gitlab
4. Gitlab 部署
5. …………hugo 新建文章，部署
6. hugo 软装，修改样式


至于其他功能
  - `搜索` 
  - `评论` 
  - `分类` 
  可日后增添

首要先把毛坯房搭建好



>踩到的坑有

- homebrew
- bash or zsh
- git remote



## 一 搭建: Hugo 
终端 iterm2 执行命令
`~ brew install hugo`
发现：
`Updating Homebrew...`

解决问题“如何让 homebrew 更新时间缩短”

[[How] [homebrew] update 问题解决 (#17) · Issues · qilin / qilin_craft.gitlab.io · GitLab](https://gitlab.com/qilin_craft/qilin_craft.gitlab.io/-/issues/17)
~ 两个方案：
－ ctrl+c 中断更新
－ 替换成国内清华镜像 （有坑无数待补）

homebrew 过程中, 需要安装  `xcode` [[Q] Xcode Command Lines Tool 是什么/ 为什么 homebrew 的时候要安装 (#16) · Issues · qilin / qilin_craft.gitlab.io · GitLab](https://gitlab.com/qilin_craft/qilin_craft.gitlab.io/-/issues/16#note_846257570)


安装完毕, 继续 `~ brew install hugo`

### 报错 1 : A newer Command Line Tools release is available

```
Error: A newer Command Line Tools release is available.
Update them from Software Update in System Preferences or run:
  softwareupdate --all --install --force

If that doesn't show you an update run:
  sudo rm -rf /Library/Developer/CommandLineTools
  sudo xcode-select --install

Alternatively, manually download them from:
  https://developer.apple.com/download/more/.
```

原因: 
1. 软件待升级
2.  如果不是升级问题, 执行以下命令
   ```
   sudo rm -rf /Library/Developer/CommandLineTools`
   sudo xcode-select --install
   ```
3. 或者 https://developer.apple.com/download/more/  下载 Xcode

#### 尝试 101 : 升级软件

>`softwareupdate --all --install --force`

注: 其实不应该升级,因为刚安装, 一定不是这个问题; 

- 以下运行结果证实这个行为无效
  - 因为此命令升级了所有软件, 包括与此项目无关的 Safari 浏览器


	```
	Downloading Device Support Update
	Downloading Command Line Tools (macOS Mojave version 10.14) for Xcode
	Downloading Safari 浏览器
	Downloaded Safari 浏览器
	Installing Device Support Update, Command Line Tools (macOS Mojave version 10.14) for Xcode, Safari 浏览器
	DONE
	```


继续执行 `~brew install hugo`

- 又出现同样报错结果

#### 尝试 102 : 重新下载 Command Line Tools for Xcode

> 查询后论坛说有可能是个 bug, 是 homebrew 的 bug

>- homebrew 都嫌弃 Mojave 了， 只能被迫升级到 macOS Catalina 10.15.7 
>- 我看 homebrew 的说法，似乎只要没有 bottle 的自行构建全都是 unsupported

不管以上, 尝试重新下载 Command Line Tools for Xcode 11.3.1

1. 登录  https://developer.apple.com/download/all/.
2. 填写 Apple ID  wangjie@gmail.com  密码  app store 密码 59@A
3. 登入 https://developer.apple.com/download/all/
More - Downloads - Apple Developer

4. download the Command Line Tools for Xcode 11.3.1

查看是否安装成功, 成功;

	```
	  ~ cd /Library/Developer/CommandLineTools`

	   或者
	  ~ gcc -v
	```


   显示信息:

  
   	```
	Configured with: --prefix=/Library/Developer/CommandLineTools/usr --with-gxx-include-dir=/Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/c++/4.2.1
	Apple clang version 11.0.0 (clang-1100.0.33.17)
	Target: x86_64-apple-darwin18.7.0
	Thread model: posix
	InstalledDir: /Library/Developer/CommandLineTools/usr/bin
	```

  但是, 重新执行 `~ brew install hugo` 
     
  继续报错
  

	```
	Error: The contents of the SDKs in your Command Line Tools (CLT) installation do not match the SDK folder names.
	A clean reinstall of Command Line Tools (CLT) should fix this.

	Remove the broken installation before reinstalling:
	  sudo rm -rf /Library/Developer/CommandLineTools

	Install the Command Line Tools for Xcode 11.3.1 from:
	  https://developer.apple.com/download/all/
	```


 报错原因:

 - 由于安装了两个 Xcode CIT ,
   -  SDKs 内容和文件夹名字不匹配

 - 需要移除再重新下载

```
	  ~ sudo rm -rf /Library/Developer/CommandLineTools # 移除

```


- 继续安装刚刚下载的 dmg

-->成功 

#### 最终尝试 brew install hugo

虽然出现同样的警告⚠️, 但是却执行下去继续安装 hugo

```
Warning: You are using macOS 10.14.
We (and Apple) do not provide support for this old version.
You will encounter build failures with some formulae.
Please create pull requests instead of asking for help on Homebrew's GitHub,
Twitter or any other official channels. You are responsible for resolving
any issues you experience while you are running this
old version.

==> Downloading https://github.com/gohugoio/hugo/archive/v0.92.2.tar.gz
Already downloaded: /Users/apple/Library/Caches/Homebrew/downloads/4918e843e13c26bdf338afbf47d8b4e975cbdaae994286bff0a54ddb24b26389--hugo-0.92.2.tar.gz
==> go build -ldflags=-s -w -tags extended
==> /usr/local/Cellar/hugo/0.92.2/bin/hugo gen man
==> Caveats
Bash completion has been installed to:
  /usr/local/etc/bash_completion.d
==> Summary
🍺  /usr/local/Cellar/hugo/0.92.2: 48 files, 54.8MB, built in 4 minutes 37 seconds
==> Running `brew cleanup hugo`...
Disable this behaviour by setting HOMEBREW_NO_INSTALL_CLEANUP.
Hide these hints with HOMEBREW_NO_ENV_HINTS (see `man brew`).
Removing: /Users/apple/Library/Caches/Homebrew/hugo--0.53.tar.gz... (19.9MB)

```

#### 测试: ~ hugo version
>`~ hugo version`

显示

`hugo v0.82.0+extended darwin/amd64 BuildDate=unknown `

说明 Hugo 安装成功

### 总结

遇到的大坑主要是 `更新`

- homebrew 更新
  - You are using macOS 10.14.We (and Apple) do not provide support for this old version.
    - 要不就按照提示更新软件
    - 要不就忽略, 可能真是 bug
- xcode CIT 不断提示更新
  - 下载了两次, 安装了两次
  - 可是为什么刚下载的提示更新, 可能原因
    - 安装源不同:
    + 一是通过 brew , 但凡跟 brew 涉及的都有可能遇到更新问题
    + 二是 官网 app store / https://developer.apple.com/download/more/  下载
    - 第二种方法可靠, 因为跟 brew 无关, 最终也是靠这种方法成功的

 #### 经验

 - homebrew 更换成国内安装源, 速度会加快
   - 如果还出问题, 不要用 brew 下载
   - 改成其他下载工具, 如

 - 遇到下载两次的, 要卸载掉第一次下载的软件
   





