---
title: "技术 | git 命令 git add/commit/push 合并为一条命令"
date: 2022-03-17T22:45:15+08:00
tags: ["技术"]
keywords: []

---
## 总结
- cd qi 的时候就点击 tab 键，自动补齐文件夹名
- 新建文章 : 中文状态下输入 new ,选择 2， 出现`hugo new posts/$(date +"%Y-%m-%d").md`
- git add/commit/push 合并：cd qilin.io 后 ，输入 `lazygit "commit 的内容"`自动上传


## hugo new  新建文章

>hugo new posts/2022-03-16.md

- 快捷键：搜狗-高级-自定义短语-new - 第二位

`hugo new posts/$(date +"%Y-%m-%d").md`

## 合并命令

参考 原文链接：[git add, commit and push commands in one?](https://stackoverflow.com/questions/19595067/git-add-commit-and-push-commands-in-one)



1. 背景
=====

在使用 git 的时候，经常需要使用下面三条语句，即使有时候只改动了一点，

```
git add .
git commit -a -m "My commit msg."
git push
```

所以希望有方法可以用一条语句实现以上三个命令。

2. 方法
=====

在. bashrc（如果是 Mac，则添加. bash_profile）中添加 _**lazygit**_ 函数，而不是使用别名，这样的话允许传递参数。

_**注意：这里我的 iterm 是 zsh ,所以 . bashrc 代替为 ~/.zshrc**_

```
function lazygit() {
    git add .
    git commit -a -m "$1"
    git push
}
```



**_lazygit_** 函数如下：

```
function lazygit() {
    git add .
    git commit -a -m "$1"
    git push
}
```

添加完成之后运行：

```
source ~/.bashrc
```

现在可以使用以下命令上面的完成三条语句：

```
lazygit "My commit msg."
```

当然可以通过接受更多的参数来加强这一点







## 具体步骤

```
➜  ~ vi ~/.zshrc
➜  ~ source ~/.zshrc
compinit:498: no such file or directory: /usr/local/share/zsh/site-functions/_brew_cask
➜  ~ cd qilin_craft.gitlab.io
➜  qilin_craft.gitlab.io git:(main) ✗ lazygit "My commit msg."
[main 6850bf0] My commit msg.
 3 files changed, 33 insertions(+)
 create mode 100644 content/posts/%NumberDate%.md
 create mode 100644 content/posts/16-03-2022.md
 create mode 100644 content/posts/2022-03-16.md
Counting objects: 7, done.
Delta compression using up to 4 threads.
Compressing objects: 100% (7/7), done.
Writing objects: 100% (7/7), 790 bytes | 0 bytes/s, done.
Total 7 (delta 2), reused 0 (delta 0)
To gitlab.com:qilin_craft/qilin_craft.gitlab.io.git
   02e516d..6850bf0  main -> main
```

成功

## 其他 如何添加 vim/vi

![](https://tva1.sinaimg.cn/large/e6c9d24egy1h0c3zsix39j213c0iawhr.jpg)


## Changlog
- 2022-03-17 init



