---
title: "技术 | HP 打印机无线设置"
date: 2022-03-08T10:49:46+08:00
tags: ["技术"]
keywords: []
description: ""
slug: ""
---

## 前缘
想实现

- 打印 issue , 贴在白板, 追踪议题

- 打印 writeathon 卡片, 主要是文学方面的词句卡和片段卡, 规格 4*6 英寸
- 打印需要重点精读的文件逐字逐句阅读

## 背景
~ 设备: HP DeskJet Ink Advantage 3700 All-in-One Printer series

~ 无线打印, 更换网络为 `DeskJet Ink Advantage 3700` 即可手机/电脑实现打印

~ 换环境需要重新设置打印机网络

故记录步骤


## 步骤
### 0. 重要参考文件
打印机信息自动打印的两份文件:

- 无线快速入门手册(主要参照此文件步骤)
![](https://tva1.sinaimg.cn/large/e6c9d24egy1h02gqrdetdj21hc0u0whv.jpg)
- 打印机信息
![](https://tva1.sinaimg.cn/large/e6c9d24egy1h02gqsn80bj21hc0u0di7.jpg)


### 1. 如果未安装 hp smart, 需要先安装






- 00: 确保您的计算机或移动设备与打印机连接到相同的网络，这样才能在软件中添加打印机
第一使用，此前未安装过打印机软件的移动设备或计算机。
- 1. 在打印机上按下无线按钮和取消按钮3秒钟。
- 2. 打开浏览器访问 [123.hp.com/di3779已安装惠普软件](http://123.hp.com/di3779%E5%B7%B2%E5%AE%89%E8%A3%85%E6%83%A0%E6%99%AE%E8%BD%AF%E4%BB%B6) ，按照说明操作。
 
![](https://tva1.sinaimg.cn/middle/e6c9d24egy1h02h6inx43j214k0qwq6h.jpg)

- 3.输入产品型号


- 4.安装 hp smart



        * 跳到 app store
        * 连接墙网
        * 安装


- 5.设置新打印机
![](https://tva1.sinaimg.cn/middle/e6c9d24egy1h02h70pqzhj21gi0u00v3.jpg)
![](https://tva1.sinaimg.cn/middle/e6c9d24egy1h02h6nt2lhj21am0q2jte.jpg)
- 6.连接无线网络后
![](https://tva1.sinaimg.cn/middle/e6c9d24egy1h02h6lysrcj20qk0daq32.jpg)




- 7.最后怎么判断连接成功: 无限指示灯停止闪烁,并保持点亮状态
## 使用测试
- 网络选择 hp
- 出现感叹号即能打印 (误以为`!`是不能打印,又鼓捣了半天)
![](https://tva1.sinaimg.cn/large/e6c9d24egy1h02h6j5ffkj20ji06gaaj.jpg)

### hp smart 中创建账户, 方便手机也能用

- 创建账户 
    - qq 邮箱, 不敢用 gmail . 战争封 gmail 账号
    - 密码需要包含大写字符






## 安卓手机打印方法

### 1. edge 浏览器 打印网页比如 `我来` 笔记
### 2. wps 打印 pdf 

### 3. remnote 打印

### HP smart 照片打印
1. [HP Smart - Free - download for Android](https://downloads.digitaltrends.com/hp-smart/android/post-download) 下载安卓版本
2. 手机端安装 , 70 M
3. 登入账户 (160qq.com)
4. 选择照片打印即可

出现的几个问题
#### 1. 登入账户的时候自动连接小米自带浏览器, 无法登陆

--> 这时只要把登陆网址复制到 edge 浏览器, 在 edge 中登陆, 不到一秒就可登陆, 
-->Hp smart 显示账户登陆成功.

此时选择打印照片或者其他即可

注意: 照片打印只能选择 A4尺寸

#### 2. 第二个问题是, 不要添加打印机. 
之前踩过的坑是"添加打印机"发现怎么都连不上网络,连上网络后需要无线密码, 然而怎么都登不上.

错误记录如下

#### 3. 需要安装 hp 打印机插件, 应用商店自动下载

- 蓝色图标





## Changlog
- 2022-03-08 init



