
---
title: "技术 | hugo 发布"
date: 2022-03-08T08:49:46+08:00
tags: [" 技术"]
keywords: []
description: ""
slug: ""
---

handbook:
1. cd qil(敲击 tab)
2. 搜狗中文输入 new: hugo new posts/当日日期.md
3. lazygit "输入 commit 的内容": 合并了 git add . 等命令

- tips:
    - 参考[\[Handbook\]博客上传发布快捷命令，包括 git add 合并 (#92) · Issues · qilin / qilin\_craft.gitlab.io · GitLab](https://gitlab.com/qilin_craft/qilin_craft.gitlab.io/-/issues/92)

end


## 0. 新建文章: hugo new posts/my-first-post.md


或者直接在 mweb 编辑器的 gitlab.io/content/posts 中添加新文章, 直接修改文件名, copy 之前文件中的头部配置信息, 但这种方法有风险，不知道哪里会触发格式不对

最好在命令窗口输入命令

~ hugo new posts/2022-03-17.md

因为麻烦，所以将命令赋值于 “new”：搜狗输入法自定义短语 , 中文输入法下敲击 new ,选第二位
```
hugo new posts/$(date +"%Y-%m-%d").md
```




### 0.1文件名: 2022-03-07-MyFirstPost.md
>好例子:2022-03-07-MyFirstPost.md

>坏例子:  Dairy-2022-03-07, 无法显示

格式:

- 必须以数字开头
- `-`英文短横杠为分割
- 英文名最好首字母大写,不要分隔符


### 0.2 文件位置

>该文件位于 xxx.gitlab.io/content/post

### 0.3 元数据-头部配置信息

~ 进入编辑页面, 自动添加头部配置信息如下:

```
---
title: "Dairy | 220228-220306"
date: 2022-03-07T10:49:46+08:00
draft: true
tags: ["日记"]

---
```
~ 头部配置信息模板修改
  - 配置文件: xxx.gitlab.io/archetypes/default.md 

```
  ---
title: "{{ replace .Name "-" " " | title }}"
date: {{ .Date }}
draft: true
tags: [""]
keywords: []

---

```	



#### 信息说明
  	
- title: 博客网站显示的文章名, 和文件名可以不同; 
- date: 自动添加
  - 如果不是通过 hugo new 的命令, 需要手动改日期??如何改
- draft: 默认 true; 即为草稿状态, 此状态即使托管到 gitlab, 也不会在博客显示;
  - false 或者手动删除 draft 一行, 即可以发布
  - 善于利用 draft, 不想发布但想托管的文章可以用 draft
- tags: 创建标签; 
  - 配置文件 config.toml 里可以编辑
    - ![](https://tva1.sinaimg.cn/middle/e6c9d24egy1h01gp2lv4yj20n804kmxa.jpg)
  - 位置: xxx.gitlab.io/public/tags/标签



## 1. 发布文章

> 1.1 本地查看: ~ hugo server 或者 hugo server -D
> 1.2 发布网页: 
	> ~ git add . 
	> ~ git commit -m ""
	> ~ git push
> 以上命令合并为一步：~ lazygit "xxx"

~ 重点是 1.1 本地查看调整草稿的内容



- `hugo server` : 本地文件的网页渲染; 无需发布即可实时查看
	- 当想查看格式怎么样, 就启动这一命令, 看看网页端渲染是不是想要的效果
	- 不包括草稿
	- 点击命令窗口里的 [http://localhost:1313] 即在浏览器中出现网页渲染
	- 退出时 control+c
- `hugo server -D`: 本地查看草稿

## 2.其他:设置文章过期时间/发布时间

在元数据-头部信息中, 添加

```
expiryDate：2019-12-18T21:00:20+08:00 #过期时间，已过期的内容不会渲染到网页中

publishDate：2019-12-18T21:00:20+08:00 #发布时间，未到发布时间不会渲染到网页中
```

上述两类文章如需发布, 在 hugo 命令后添加以下参数

~ hugo -E #—buildExpired
~ hugo -F #—buildFuture

推送 gitlab 后就会看到.
### 2. 查看网页渲染
发现一个问题: 发布后登入 主页[QI Lin ｜ Oh My Brain](https://qilin_craft.gitlab.io/), 并没有最新文章更新

分析:以下信息是否有问题
- 文件名
- 头部信息
- 文件位置

~ 如果都没问题

- 在分类 `essay` 或者 `tag` 中查看有没有最新文章
- 最可能在 tag 下有, 比如这次点击 `cook` 标签才发现最新文章"早餐吃肉系列"




end

## Changlog

- 2022-03-08 fix









