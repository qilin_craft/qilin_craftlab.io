---
title: "2022 03 16 Cook JiTui"
date: 2022-03-16T14:54:15+08:00
tags: [""]
keywords: []

---
# 背景
今天做了一下炸鸡腿，发现重点和难点在于鸡腿的处理

鸡腿有血丝，耗了很长时间，要不出血水来血水仍然很多，怎么办

如何剃鸡骨头

这样的问题可以合成一个问题，如何处理鸡大腿

## 步骤

1. 去血水
>还是遵照洗排骨的方法，用盐和小苏打搓洗，可以加少量水。然后水冲洗或者半个小时内换水两三次。
  
- 冷冻时间越久的鸡腿越有一种挥之不去的腥味，所以建议去大超市买冰鲜鸡腿而不是冷冻鸡腿。

2. 剔骨（可选）/ 剁成小块（看情况）

3. 切刀，切深![](https://tva1.sinaimg.cn/small/e6c9d24egy1h0bp9h9w5ej20qc0k43zv.jpg)

3. 酱汁做按摩充分吸收
   - 不要放含糖的调料，比如蜂蜜，老抽，蚝油，会糊锅
4. 腌制：保鲜膜密封入味

5. 擦干净酱汁，否则糊锅

6. 爆姜丝

7.  下鸡腿：一定要煎出焦脆感，再进行下一步



### 剔骨

- 用小刀不要用切刀, 弹幕说用剪刀更容易。


![](https://tva1.sinaimg.cn/small/e6c9d24egy1h0bpkuenr9j20vc0n8jul.jpg)

- 翻面，刀紧贴骨头插进去,挑断关节脆骨

![](https://tva1.sinaimg.cn/small/e6c9d24egy1h0bpma9zgzj20oq0pugnx.jpg)

- 竖起来，用刀背刀背刀背把肉刮下来

![](https://tva1.sinaimg.cn/small/e6c9d24egy1h0bpneuah8j20uo0n077f.jpg)
- 剔好后是这样的

![](https://tva1.sinaimg.cn/small/e6c9d24egy1h0bpobmqa9j20sq0sg0wq.jpg)

### 煮高汤
>撇去浮沫是一切汤的重点重点重点
>大火是重点重点
>鸡爪鸡翅尖的加入可以大大增加鸡汤的浓郁程度。


汤头会浓郁，不在于那两根小骨头的汤，那个不是重点！！

>关键点在于1️⃣ 油脂和开水的冲撞 2️⃣ 大火让脂肪和水不断的碰撞包容（大火熬浓汤，小火炖清汤。）所以如果你买了去骨的鸡腿，那炒完鸡肉倒入开水，用大火炖煮，一样可以做出浓郁汤头！！！

- 冷水煮，骨头上的血不用管
- 八角小葱花椒

- 大火煮沸后撇去浮沫

### 煎鸡皮
- 中小火，平底锅不放油

![](https://tva1.sinaimg.cn/small/e6c9d24egy1h0bpr4kaddj20zk0iwacy.jpg)

- 最重要的是不要翻动，不要翻动，耐心等五分钟。鸡皮逼出大量油脂
![](https://tva1.sinaimg.cn/small/e6c9d24egy1h0bpsq582cj20s20lm76q.jpg)
- 如果糊锅，说明没有擦干净酱汁儿或者加了糖
- 煎好后，乘出来，鸡皮朝下，切成你喜欢的大小。
![](https://tva1.sinaimg.cn/small/e6c9d24egy1h0bpu562h2j20p60jqdhk.jpg)

~ tips

```
鸡皮煎不黄的原因可能是：

1、鸡皮太少

（客观原因，可能买的鸡皮就掉。还有就是去骨的时候，注意保护我方鸡皮）

2、火太大还来及出油就焦了

3、火太小逼不出来油

最好用中小火，火候一定要重点把控，切忌开中火、大火，切忌着急。

4、鸡皮没有擦干水分，水油混合容易粘锅。

但是鸡皮煎不黄不一定会翻车，因为成败的关键点是鸡皮能否煎出油

```

### 处理鸡腿后下一步
![](https://tva1.sinaimg.cn/small/e6c9d24egy1h0bpvnbakxj20ts0majtb.jpg)

~ 如果做香菇鸡汤参考[知乎15万收藏！2个小鸡腿，15分钟科学高效的做出超香浓鸡汤！_哔哩哔哩_bilibili](https://www.bilibili.com/video/BV1LK411M7NE?spm_id_from=333.337.search-card.all.click)
- 鸡肉变色后给盐给料酒去腥味
- 倒入鸡骨汤
- 做火锅米线，面条的汤底也非常不错。

## 参考

* [鸡腿_搜索_哔哩哔哩-bilibili](https://search.bilibili.com/all?vt=11666600&keyword=%E9%B8%A1%E8%85%BF&from_source=webtop_search&spm_id_from=333.788)
* [厨师长教你：“番茄土豆烧鸡腿”的下饭做法，咸鲜微酸，家常美味_哔哩哔哩_bilibili](https://www.bilibili.com/video/BV12341177nh?spm_id_from=333.337.search-card.all.click)
* [不加一滴水的蒜香鸡腿！肉嫩多汁，我敢保证吃一次就会爱上.蒜蓉西兰花_哔哩哔哩_bilibili](https://www.bilibili.com/video/BV1tK41177xD?spm_id_from=333.337.search-card.all.click)
* [鸡腿这样做实在太香了，比红烧的好吃，比油炸的健康，出锅不够吃_哔哩哔哩_bilibili](https://www.bilibili.com/video/BV1444y157xG?spm_id_from=333.337.search-card.all.click)
* [知乎15万收藏！2个小鸡腿，15分钟科学高效的做出超香浓鸡汤！_哔哩哔哩_bilibili](https://www.bilibili.com/video/BV1LK411M7NE?spm_id_from=333.337.search-card.all.click)

- 烟的时候不要放含糖的调料，比如蜂蜜，老抽，蚝油。
- 验完之后要记得把酱汁都擦干净。没有不然会糊锅。
```
腌制的调料

因为煎的时间足够长，所以腌制的时候一定不能放糖，蜂蜜，老抽，或者蚝油。

它们是会赋予鸡肉很好的颜色是没错。

但是！！！煎一小会儿就会糊锅，鸡皮粘的都烂烂的。

鸡皮煎出来应该是焦的，是脆的，是香的！

## Changlog
- 2022-03-16 init



