---
title: "技术 | gitlab issue 如何导出为单个 md 文档"
date: 2022-03-16T22:18:26+08:00
tags: ["技术"]
keywords: []

---
- Related to [\[How\]issue 如何导出 (#11) · Issues · qilin / qilin\_craft.gitlab.io · GitLab](https://gitlab.com/qilin_craft/qilin_craft.gitlab.io/-/issues/11)
- 参考 [ Python3 将 Gitlab issue 导出到 markdown_昵称就这样行吧的博客-CSDN博客](https://blog.csdn.net/github_25176023/article/details/105737191)
# issue 如何导出

## 0. 操作步骤 Handbook4issueCsv2md
每次issue 从 csv 导出 md 步骤

1. 激活虚拟环境 ~source env/bin/activate
2. 进入备忘笔记文件夹 ~cd /Users/apple/Dropbox/备份笔记
3. 执行 ~python3 issue_to_markdown.py
4. 检查 备忘笔记/out 文件夹是否转化成功
5. 退出虚拟环境：deactivate

## 总结第一次安装环境正确步骤

配置环境，安装 tqdm & requests库， 两者都要在 python3 虚拟环境下

所以要先进入虚拟坏境

1. ~ python3 -m venv env #创建虚拟环境（已经创建就忽略此步）
2. ~ source env/bin/activate #激活虚拟环境, 以后每次都要进来执行文件
3. 安装 tqdm :~python3 -m pip install tqdm
4. 检查安装成功否？~python3 -m pip show tqdm
5. 安装 requests库：~pip3 install requests
6. 进入文件所在文件夹： cd xx(坚果云/备份笔记)
7. 运行:~python3 issue_to_markdown.py
8. 退出虚拟环境：deactivate





## 一. 导出 csv
- 可以按标签或者搜索选项导出，比如搜索[人文]下的 issue, 只导出人文

![image](/uploads/39847731b0eb12591f823db8755f1e1b/image.png)
## 二.如何导出 md 格式
###  尝试
[ Python3 将 Gitlab issue 导出到 markdown_昵称就这样行吧的博客-CSDN博客](https://blog.csdn.net/github_25176023/article/details/105737191)

~ 核心的方法是用gitee 提供的api接口来获取issue和comments里的markdown字符串。

glpat-Swmf2H-rurXTGdscico7


## 步骤
### 1. id
![](https://tva1.sinaimg.cn/middle/e6c9d24egy1gzkwvj2hwlj20i8078jrm.jpg)
### 2. token
![](https://tva1.sinaimg.cn/middle/e6c9d24egy1gzkwwi9ab6j20xm0oydj5.jpg)

### 3. 执行 python

- 将issue_to_markdown.py 保存在 坚果云-备份笔记下

- 复制以下内容至 issue_to_markdown.py


```
from tqdm import tqdm
import os
import argparse
import codecs
import json
import re
import requests
requests.session().keep_alive = False  # 及时释放


def get_info(api_url, token):
    headers = {'Private-Token': "%s" % token}
    r = requests.get(
        api_url, headers=headers)
    ret = json.loads(r.text)
    if r.status_code > 300:
        print('error %s', r.text)
        return False
    return ret


def to_markdown(page, mk_note):
    mk = '# ' + page['title'] + '\n'
    mk += "created_at: "+page['created_at']+"\n"
    mk += "updated_at: "+page['updated_at']+"\n"

    labels = "label: "
    for idx, item in enumerate(page['labels']):
        if (idx+1) == len(page['labels']):
            labels += item+'\n'
        else:
            labels += item+','
    mk += labels+'\n'

    mk += page['description']+'\n'

    mk += mk_note
    return mk


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-p', '--id', help="gitlab project id")
    parser.add_argument('-t', '--token', help="gitlab personal access token")
    parser.add_argument('-o', '--dir', help="out put dir")

    args = parser.parse_args()

    # args.id = 33740781
    # args.token = "glpat-S6G4qr3DLCdQ_HXycy7q"
    # args.dir = "out"

    if not os.path.exists(args.dir):
        os.makedirs(args.dir)

    base = "https://gitlab.com/api/v4"
    # 当然你也可以自建gitlab

    issue_url = base+"/projects/%d/issues?per_page=%d"
    # gitlab默认服务器端每次只返回20条数据给客户端，可以用设置page和per_page参数的值，指定服务器端返回的数据个数。

    note_url = base+"/projects/%d/issues/%d/notes?sort=asc"

    # 包含了所有的issue both open and closed
    ret = get_info(issue_url % (args.id, 100), args.token)

    for page in tqdm(ret):
        if page['state'] == 'opened':  # 去掉关闭的issue
            _pid = page['project_id']
            _iid = page['iid']

            mk_note = ""  # 获取评价
            if page['user_notes_count'] > 0:
                notes = get_info(note_url % (_pid, _iid), args.token)
                for note in notes:
                    if not note['system']:  # 过滤系统的notes
                        mk_note += note['body']+'\n'

            mk = to_markdown(page, mk_note)

            # save
            filename = page['created_at'].split('T')[0]+','+page['title']+'.md'
            filename = re.sub(r'[/:*?"<>|]', " ", filename)  # check filename
            filename = os.path.join(args.dir, filename)

            with codecs.open(filename, 'w', 'utf-8') as file:
                file.write(mk)


```

- 修改内容赋值
   - 将以下三行去掉 `#` 直接赋值，把自己 gitlab-id/token/out 粘贴过去

```
# args.id = xxxx
# args.token = "xxx"
# args.dir = "out"

```


- cd 目录
   - `~ python3 issue_to_markdown.py`



### 报错:tqdm没安装
```File "issue_to_markdown.py", line 1, in <module>
    from tqdm import tqdm
ModuleNotFoundError: No module named 'tqdm'
```

~ 查询 tqdm

[Python 超方便的迭代进度条 (Tqdm)-Python 实用宝典](https://pythondict.com/life-intelligent/tqdm/)



- 是进度条? 可以不安装吗?

~ 解决方案

1.
```
 ~ python -m pip list
Package    Version
---------- -------
chardet    3.0.4
pip        18.0
setuptools 40.4.3
wheel      0.32.0
zhpy       1.7.4

```

-> 没有安装 Tqdm

2. 安装

```
~ pip install tqdm
Successfully installed tqdm-4.62.3
```

__> 报错

`ModuleNotFoundError: No module named 'tqdm'`

~ 查询解决方案
`Are you absolutely certain the CLI and Notebook are running in (pointing to) the same environment?`


#### 分析：tm 可能没安装在 python3 下
~ 参考正确的 [How to Solve Python ModuleNotFoundError: no module named 'tqdm' - The Research Scientist Pod](https://researchdatapod.com/python-modulenotfounderror-no-module-name-tqdm/#:~:text=A%20common%20error%20you%20may%20encounter%20when%20using,Python%203%20with%20python%20-m%20pip%20install%20tqdm.)
1. 卸载 刚刚装的 tm
`pip uninstall tqdm`

2. 创建虚拟 python3
>first create the virtual environment:

`python3 -m venv env`

Then activate the environment using:

`source env/bin/activate `

![](https://tva1.sinaimg.cn/small/e6c9d24egy1h0d6ovlf3ij20ik05mjrr.jpg)

`python3 -m pip install tqdm`


3. Check tqdm Version

`python3 -m pip show tqdm`

```
(env) ➜  ~ python3 -m pip show tqdm
Name: tqdm
Version: 4.63.0
Summary: Fast, Extensible Progress Meter
Home-page: https://tqdm.github.io
Author: None
Author-email: None
License: MPLv2.0, MIT Licences
Location: /Users/apple/env/lib/python3.7/site-packages
Requires:
Required-by:
```

### 报错  requests 没安装
#### 安装requests库
>Requests库是python的第三方库，它也是目前公认的爬取网页最好的第三方库，它有两个特点，很简单也很简洁，甚至用一行代码就可以从网页上获取相关资源。

- MAC在基于python3中安装requests库最方便，而Mac上自带的是python2，就需要先在Mac上安装python3，
下面介绍一下Requests库的安装方法

- 因为我上一步已经进入 py3 虚拟坏境，如何判断？
   - iterm2 显示：（env）->
- 如果没有进入，重新执行

```
Then activate the environment using:

`source env/bin/activate `

```

- 在终端输入

`pip3 install requests`

- 成功



### 最后 执行 csv->md

- cd 文件夹
- python3 issue_to_markdown.py



## Changlog
- 2022-03-17 2h


