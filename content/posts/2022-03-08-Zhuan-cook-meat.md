---
title: "转载 | 蒸肉饼: 山药莲藕马蹄"
date: 2022-03-07T10:49:46+08:00
tags: ["cook"]
keywords: []
description: ""
slug: ""
---


> 本文由 [简悦 SimpRead](http://ksria.com/simpread/) 转码， 原文地址 [www.xiachufang.com](https://www.xiachufang.com/recipe/106620365/)

> 1. 我用这种梅头肉做肉饼；2. 三分肥七分瘦的上肉或者梅头肉剁成肉馅，用绞肉机打的话几秒钟就可以，不用打太细，粗一点口感更好；3. 一小段山药切成碎，从削皮开始一定要带上一次性手套，碰到手背毛孔真的是太痒了。

![](https://i2.chuimg.com/90a5d1528efe4385b4ea62023ac40a2c_1080w_1082h.jpg?imageView2/2/w/660/interlace/1/q/90)

这道[家常菜](https://www.xiachufang.com/category/40076/)很适合 1 岁以后的小朋友吃，孩子不爱吃肉，或者吞咽不下，又不吃辣椒，这个肉饼简单易做又有营养，好吃不上火

用料  
----

孩子爱吃的蒸肉饼的做法  
-------------

1.  我用这种梅头肉做肉饼
    
    ![](https://i2.chuimg.com/01e5aec2e27c4897aa83b862a20074b3_1368w_996h.jpg?imageView2/2/w/300/interlace/1/q/90)
2.  三分肥七分瘦的上肉或者梅头肉剁成肉馅，用绞肉机打的话几秒钟就可以，不用打太细，粗一点口感更好
    
    ![](https://i2.chuimg.com/3ee26fbde3214626be8839644dd906c3_684w_912h.jpg?imageView2/2/w/300/interlace/1/q/90)
3.  一小段山药切成碎，从削皮开始一定要带上一次性手套，碰到手背毛孔真的是太痒了。这个步骤的配料可以换成马蹄、香菇、玉米粒、莲藕，看个人喜好，可以轮着做，我宝宝最喜欢玉米、淮山、香菇的。淮山健脾养胃的，用来煲粥、炖汤、清炒我都喜欢。
    
    ![](https://i2.chuimg.com/acffd513d79f4a4897d5fc68a5da41b8_912w_684h.jpg?imageView2/2/w/300/interlace/1/q/90)
4.  调料的勺子是普通瓷更
    
    ![](https://i2.chuimg.com/2dcb7ee3036940ffaf33d3db75b239e9_684w_912h.jpg?imageView2/2/w/300/interlace/1/q/90)
5.  肉馅和山药碎倒一起，加入淀粉 2 勺（我喜欢用老家的番薯粉）、生抽 2 勺、蚝油 1 勺、花生油 1 勺、盐适量、糖半勺、把葱头切碎放进去一起搅拌均匀，然后倒盘子里面铺平，往里加一点水，我喜欢汤汁多一点的
    
    ![](https://i2.chuimg.com/10373f6ad080433fa4761d97574bbcf9_684w_903h.jpg?imageView2/2/w/300/interlace/1/q/90)
6.  玉米馅的，用半根甜玉米就可以了，买一个剥玉米神器，几秒就剥下来了
    
    ![](https://i2.chuimg.com/3119ac145fdc425db9c1e2119bf02066_684w_912h.jpg?imageView2/2/w/300/interlace/1/q/90)
7.  调料集合
    
    ![](https://i2.chuimg.com/76a3762f9acf45f5b6c4161604c240aa_912w_684h.jpg?imageView2/2/w/300/interlace/1/q/90)
8.  水开后放蒸锅里蒸 15 分钟
    
    ![](https://i2.chuimg.com/98265fffd0b0405887c7738237073312_1080w_864h.jpg?imageView2/2/w/300/interlace/1/q/90)
9.  蒸熟后撒上灵魂葱花，开吃，滑滑嫩嫩的，加点汤汁拌饭，孩子爱吃又有营养
    
    ![](https://i2.chuimg.com/7f937ac15d344cbea74d705a626d7402_1080w_864h.jpg?imageView2/2/w/300/interlace/1/q/90)
10.  隔三差五的蒸一次，孩子的家常菜
    
    ![](https://i2.chuimg.com/3889051a2cd94584a32e28e5c7d05bfe_684w_850h.jpg?imageView2/2/w/300/interlace/1/q/90)
11.  这是玉米肉饼
    
    ![](https://i2.chuimg.com/2be6418d66a0439b9eb9715c4fd65b80_684w_912h.jpg?imageView2/2/w/300/interlace/1/q/90)

    
 
    昨天 20:07
    
    #晚餐 •2022 年 3 月 7 日# 瑶柱虾仁香菇肉饼 家属嗷嗷叫太好吃了，打分 9.3 分😘
    
     [![](https://i2.chuimg.com/5e250f8aa46a11e6bc9d0242ac110002_851w_850h.jpg?imageView2/1/w/30/h/30/interlace/1/q/90) 易易易啊易](https://www.xiachufang.com/cook/100913378/ "易易易啊易的厨房") 
    
*    [![](https://i2.chuimg.com/eb2652a3ddb949e8abd72bc2a8e3fe24_1080w_1080h.jpg?imageView2/1/w/280/h/280/interlace/1/q/90)](https://www.xiachufang.com/dish/185703776/ "孩子爱吃的蒸肉饼") 
    
    2 天前
    
    我加了玉米马蹄鱿鱼，超赞的，我一直炖肉饼我儿子都不吃，这次距吃了，我也爱吃，现在才知道蒸肉饼买的肉是梅头肉，...
    
     [![](https://i2.chuimg.com/e2681dcd04204df1b212924af5b1a2bd_132w_132h.jpg?imageView2/1/w/30/h/30/interlace/1/q/90) 怪兽家的玉子](https://www.xiachufang.com/cook/129121451/ "怪兽家的玉子的厨房") 
    

[上传你做的孩子爱吃的蒸肉饼](https://www.xiachufang.com/recipe/106620365/create_dish/)


