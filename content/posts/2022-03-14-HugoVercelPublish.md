---
title: "技术 | Hugo+Vercel 部署"
date: 2022-03-14T21:02:01+08:00
tags: ["技术"]
keywords: []

---

## 背景
- 想用自己的域名
- 想让访问速度更快

参考 [vercel是什么神仙网站？ - 知乎](https://zhuanlan.zhihu.com/p/347990778) 后选择 vercel 部署

```
- vercel是我用过的最好用的网站托管服务。本网站就是基于hexo引擎模板开发，托管在vercel上的。
- vercel类似于github page，但远比github page强大，速度也快得多得多，而且将Github授权给vercel后，可以达到最优雅的发布体验，只需将代码轻轻一推，项目就自动更新部署了。
- vercel还支持部署serverless接口。那代表着，其不仅仅可以部署静态网站，甚至可以部署动态网站，而这些功能，统统都是免费的，简直是白嫖党的福利啊！！！！！
- vercel还支持自动配置https，不用自己去FreeSSL申请证书，更是省去了一大堆证书的配置，简直是懒人的福利啊啊啊有木有！

```

 

## 1. 步骤
- 登入[Dashboard – Vercel](https://vercel.com/dashboard)后，选择中国号码验证
- gitlab 连接

- 导入站点

>Vercel 的使用十分简单，只需要使用 GitHub 登录，然后填写源码所在的 repository 地址即可，会自动识别使用的框架，并自动生成静态文件后部署。Vercel 会自动分配给你一个网址，用来预览效果




### 报错

![](https://tva1.sinaimg.cn/large/e6c9d24egy1h09kfsa375j20u20l8q68.jpg)

~ 原因：
 - 有一篇已经创建的文档格式错误，无法部署

### 尝试
- 删掉该文件后，可以 push, 且 gitlab 也部署成功


- 重新回到 vercel 修改项目名，重新部署，成功

![](https://tva1.sinaimg.cn/small/e6c9d24egy1h09kj7x35lj216a0rqdh9.jpg)
### 建议

```
DEVELOP
- Run hugo server -D to run your project locally

PREVIEW
- Push to any Git branch other than main to preview changes

SHIP
- Push to main to ship changes to production
```

### 测试





`[QI Lin ｜ Oh My Brain](https://qilin-craft-gitlab-io.vercel.app/)`



## 2. 修改域名
>setting-domins-qilin-craft-gitlab-io.vercel.app-edit


![](https://tva1.sinaimg.cn/middle/e6c9d24egy1h09kk2p94pj21iy0natbk.jpg)


- 出现域名解析DNS提示信息

![](https://tva1.sinaimg.cn/middle/e6c9d24egy1h09kl9ip6bj214o0kktah.jpg)


- 把这个信息填到新网域名解析中


![](https://tva1.sinaimg.cn/middle/e6c9d24egy1h09kmefqopj213u0i6tap.jpg)

- add 域名

![](https://tva1.sinaimg.cn/middle/e6c9d24egy1h09kmuiexyj216m0leabt.jpg)

- 紧要一步：选哪一项？

   ~ 参考 [为什么越来越多的网站域名不加「www」前缀？ - 知乎](https://www.zhihu.com/question/20414602)
   - 应该选第二项，qilin.zone 这样就不会出现`https://www.qilin.zone/` 而是去掉 www. 



- 结果最后得到 https://www.qilin.zone/
- 后悔，就这样吧

